# [1.2.0](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-web/compare/v1.1.0...v1.2.0) (2021-06-05)


### Features

* add PasswordReset & RequestPasswordReset ([3746dad](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-web/commit/3746dad718d179871f23b410d74f7dd6db9fd97a))

# [1.1.0](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-web/compare/v1.0.0...v1.1.0) (2021-06-01)


### Features

* **component:** add ImageUpload ([cf7d258](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-web/commit/cf7d25838724afb92f943be49efba5f067838062))
* **component:** add SignIn & SignUp ([c646c10](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-web/commit/c646c10389b77dd825518994968f82a232cb6ae6))
* add initial routes ([9e44e59](https://gitlab.com/grp-fantasy-draft-board/fantasy-draft-board-web/commit/9e44e59228db3113f144e5a7404511d0c5215dad))

# 1.0.0 (2021-05-02)


### Features

* **component:** add fantasy owners ([445652e](https://gitlab.com/mrbernnz/fantasy-draft-board-web/commit/445652ee459024e191f23b8c1a3919736cbcd3ff))
