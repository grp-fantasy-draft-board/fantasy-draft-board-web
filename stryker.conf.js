/**
 * @type {import('@stryker-mutator/api/core').StrykerOptions}
 */
module.exports = {
  _comment:
    "This config was generated using 'stryker init'. Please see the guide for more information: https://stryker-mutator.io/docs/stryker-js/guides/react",
  testRunner: 'jest',
  reporters: ['clear-text', 'progress', 'json', 'html'],
  htmlReporter: {
    baseDir: 'mutation/html'
  },
  jsonReporter: {
    fileName: 'mutation/mutation.json'
  },
  coverageAnalysis: 'off',
  jest: {
    projectType: 'create-react-app'
  }
};
