import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import Fade from '@material-ui/core/Fade';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Alert from '@material-ui/lab/Alert';
import { errMessages } from '../errors/inputErrMessages';

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      marginTop: theme.spacing(3)
    }
  })
);

interface AlertMessageProps {
  open: boolean;
  handleClick: () => void;
  messageType: string;
  display: string;
}

export default function AlertMessage({
  open,
  handleClick,
  messageType,
  display
}: AlertMessageProps): JSX.Element {
  const classes = useStyles();

  return (
    <Box component="div" display={display} className={classes.container}>
      <Fade in={open}>
        <Alert
          action={
            <IconButton
              aria-label="close"
              color="inherit"
              size="small"
              onClick={handleClick}
            >
              <CloseIcon fontSize="inherit" />
            </IconButton>
          }
          variant="filled"
          severity="error"
        >
          {errMessages.get(messageType)}
        </Alert>
      </Fade>
    </Box>
  );
}
