export const errMessages = new Map();
errMessages.set('email', 'Please enter a valid email address.');
errMessages.set('password', 'Please enter your password.');
errMessages.set('signIn', 'Incorrect email or password');
