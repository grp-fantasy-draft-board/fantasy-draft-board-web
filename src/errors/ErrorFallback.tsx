import React, { MouseEvent } from 'react';

interface ErrorFallbackProps {
  error: any;
  resetErrorBoundary: any;
}

export default function ErrorFallback({
  error,
  resetErrorBoundary
}: ErrorFallbackProps): JSX.Element {
  function handleClick(e: MouseEvent<HTMLButtonElement>): void {
    resetErrorBoundary(e);
  }

  return (
    <div role="alert">
      <p>Something went wrong:</p>
      <pre>{error.message}</pre>
      <button type="button" onClick={handleClick}>
        Try again
      </button>
    </div>
  );
}
