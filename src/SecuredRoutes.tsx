import React from 'react';
import { Redirect, Route } from 'react-router-dom';

interface SecuredRoutesProp {
  children: JSX.Element;
  path: string;
}

export default function SecuredRoutes({
  children,
  ...props
}: SecuredRoutesProp): JSX.Element {
  return (
    <Route
      {...props}
      render={({ location }) =>
        sessionStorage.getItem('token') ? (
          children
        ) : (
          <Redirect to={{ pathname: '/login', state: { from: location } }} />
        )
      }
    />
  );
}
