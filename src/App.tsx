import React /* useState */ from 'react';
import { ErrorBoundary } from 'react-error-boundary';
import * as reactRouter from 'react-router-dom';
import ErrorFallback from './errors/ErrorFallback';
import Header from './layout/Header';
import RoutedComponents from './RoutedComponents';
import useStyles from './useAppStyles';

export default function App(): JSX.Element {
  const classes = useStyles();
  const location = reactRouter.useLocation();
  const history = reactRouter.useHistory();

  function handleReset(): void {
    history.push(location.pathname);
  }

  return (
    <div className={classes.root}>
      <ErrorBoundary FallbackComponent={ErrorFallback} onReset={handleReset}>
        <Header />
        <RoutedComponents />
      </ErrorBoundary>
    </div>
  );
}
