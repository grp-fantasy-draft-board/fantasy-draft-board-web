import * as yup from 'yup';

const firstNameSchema = yup
  .object()
  .shape({
    firstName: yup.string().required()
  })
  .nullable();

const lastNameSchema = yup
  .object()
  .shape({
    lastName: yup.string().required()
  })
  .nullable();

const emailSchema = yup
  .object()
  .shape({
    email: yup.string().email().required()
  })
  .nullable();

const passwordSchema = yup
  .object()
  .shape({
    password: yup.string().required().min(8)
  })
  .nullable();

const signUpSubmitSchema = yup.object().shape({
  firstName: yup.string().required(),
  lastName: yup.string().required(),
  email: yup.string().email().required(),
  password: yup.string().required().min(8)
});

const signInSubmitSchema = yup.object().shape({
  email: yup.string().email().required(),
  password: yup.string().required().min(8)
});

const formSchema = new Map();
formSchema.set('firstName', firstNameSchema);
formSchema.set('lastName', lastNameSchema);
formSchema.set('email', emailSchema);
formSchema.set('password', passwordSchema);
formSchema.set('signInSubmit', signInSubmitSchema);
formSchema.set('signUpSubmit', signUpSubmitSchema);

export default formSchema;
