import React, { lazy, Suspense } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import BarLoader from 'react-spinners/BarLoader';

const PasswordReset = lazy(async () => await import('./forms/PasswordReset'));
const RequestPasswordReset = lazy(
  async () => await import('./forms/RequestPasswordReset')
);
const SignIn = lazy(async () => await import('./forms/SignIn'));
const SignUp = lazy(async () => await import('./forms/SignUp'));

// import SecuredRoutes from './SecuredRoutes';
// import FantasyOwners from './fantasyOwners';

interface Components {
  path: string;
  component: React.LazyExoticComponent<() => React.ReactElement>;
}

function RoutedComponents(): React.ReactElement {
  const routedComponents: Components[] = [
    { path: '/login', component: SignIn },
    { path: '/sign-up', component: SignUp },
    { path: '/request-password-reset', component: RequestPasswordReset },
    { path: '/password-reset-in', component: PasswordReset }
  ];

  const routes = routedComponents.map((route, idx) => {
    return <Route key={idx} path={route.path} component={route.component} />;
  });

  return <>{routes}</>;
}

export default function Routes(): React.ReactElement {
  return (
    <Suspense fallback={<BarLoader />}>
      <Switch>
        <Redirect exact from="/" to="/login" />
        <RoutedComponents />
        {/*
      <SecuredRoutes path="/fantasy-owners">
        <FantasyOwners />
      </SecuredRoutes>
      <Route exact path="/">
        <Redirect exact from="/" to="fantasy-owners"></Redirect>
      </Route>
      */}
      </Switch>
    </Suspense>
  );
}
