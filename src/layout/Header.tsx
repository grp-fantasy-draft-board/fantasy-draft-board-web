import React from 'react';
import {
  AppBar,
  Button,
  IconButton,
  Toolbar,
  Typography
} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';
import { NavLink } from 'react-router-dom';
import useStyles from '../useAppStyles';

export default function Header(): React.ReactElement {
  const classes = useStyles();

  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton
          edge="start"
          className={classes.menuButton}
          color="inherit"
          aria-label="menu"
        >
          <MenuIcon />
        </IconButton>
        <Typography variant="h6" className={classes.title}>
          Fantasy Draft Board
        </Typography>
        <Button component={NavLink} to="/sign-in" color="inherit">
          Sign In
        </Button>
        <Button component={NavLink} to="/sign-up" color="inherit">
          Sign Up
        </Button>
      </Toolbar>
    </AppBar>
  );
}
