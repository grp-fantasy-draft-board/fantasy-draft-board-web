export interface InputData {
  [name: string]: string;
}
