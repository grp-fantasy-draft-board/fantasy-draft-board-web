import React from 'react';

export interface FantasyOwnerProps {
  name: string;
  teamName: string;
}

export default function FantasyOwner({
  name,
  teamName
}: FantasyOwnerProps): JSX.Element {
  return (
    <>
      <p>{name}</p>
      <p>{teamName}</p>
    </>
  );
}
