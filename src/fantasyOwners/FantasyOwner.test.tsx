import { render, screen } from '@testing-library/react';
import React from 'react';
import FantasyOwner from './FantasyOwner';

describe('<FantasyOwner />', () => {
  let props;
  beforeEach(() => {
    props = {
      name: 'John Smith',
      teamName: 'The Go Hards'
    };

    render(<FantasyOwner {...props} />);
  });

  test('should equal 2', () => {
    const name = screen.getByText(/john smith/i);
    expect(name).toBeInTheDocument();
  });

  test('should display fantasy team name of owner', () => {
    const teamName = screen.getByText(/the go hards/i);
    expect(teamName).toBeInTheDocument();
  });
});
