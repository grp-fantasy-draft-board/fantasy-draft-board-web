import React from 'react';
import FantasyOwner from './FantasyOwner';

const fantasyOwners = [
  { name: 'Bob Doe', teamName: 'BobWhite' },
  { name: 'Joan Blair', teamName: 'Why Not Me' },
  { name: 'John Smith', teamName: 'Hungry Hungry Hippo' }
];

export default function FantasyOwners(): JSX.Element {
  return (
    <>
      <h1>Chicken Curry</h1>
      {fantasyOwners?.map((fantasyOwner, idx) => (
        <FantasyOwner
          key={idx}
          name={fantasyOwner.name}
          teamName={fantasyOwner.teamName}
        />
      ))}
    </>
  );
}
