import React from 'react';
import FantasyOwners from './index';
import { render, screen } from '@testing-library/react';

describe('<FantasyOwners />', () => {
  beforeEach(() => {
    render(<FantasyOwners />);
  });

  test('should have a title of the fantasy league', () => {
    const title = screen.getByText(/chicken curry/i);
    expect(title).toBeInTheDocument();
  });
});
