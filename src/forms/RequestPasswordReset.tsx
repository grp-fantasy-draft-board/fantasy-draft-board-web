import React from 'react';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ClassNameMap } from '@material-ui/core/styles/withStyles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import * as hooks from './hooks';
import { InputData } from '../interfaces/InputData';
import useStyles from './styles/useFormStyles';

export default function RequestPasswordReset(): JSX.Element {
  const classes: ClassNameMap = useStyles();
  const initialInputState: InputData = { email: '' };
  const [inputs, handleChange, handleSubmit] = hooks.useForm(
    initialInputState,
    '/reset-password-request'
  );
  const [error, handleBlur] = hooks.useInputValidate(inputs, initialInputState);
  const isDisabled: boolean = hooks.useFormValidate(inputs, 'email');

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Request a password change
        </Typography>
        <form
          className={classes.form}
          autoComplete="off"
          onSubmit={handleSubmit}
        >
          <TextField
            required
            error={Boolean(error.email)}
            fullWidth
            autoFocus
            variant="outlined"
            id="email"
            name="email"
            type="email"
            label="Email"
            placeholder="Email"
            margin="normal"
            value={inputs.email}
            onChange={handleChange}
            onBlur={handleBlur}
            helperText={error.email}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={isDisabled}
          >
            Submit
          </Button>
        </form>
      </div>
    </Container>
  );
}
