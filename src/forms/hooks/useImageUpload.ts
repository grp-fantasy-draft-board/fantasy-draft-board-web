import { ChangeEvent, useState } from 'react';
import axios from 'axios';
import { api } from '../../httpClient';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function useImageUpload(directory: string, endpoint: string) {
  const initialState = undefined;
  const [profileImage, setProfileImage] = useState<string | undefined>(
    initialState
  );

  interface PresignedImage {
    signedRequest: string;
    url: string;
  }

  async function fetchPresignedImage(
    file: File | null | undefined
  ): Promise<PresignedImage | undefined> {
    try {
      const imageType = file?.type.substr(6);
      const {
        data: { signedRequest, url }
      } = await api.get(
        `${endpoint}?directory=${directory}&fileType=${imageType}`
      );

      return { signedRequest, url };
    } catch (err) {
      // TODO: update ui handling
      console.log(err);
    }
  }

  async function uploadImage(
    signedRequest: string | undefined,
    file: File | null | undefined
  ): Promise<void> {
    try {
      if (signedRequest) {
        await axios.put(signedRequest, file, {
          headers: { 'Content-Type': file?.type ?? 'image/*' }
        });
      }
    } catch (err) {
      // TODO: update ui handling
      console.log(err);
    }
  }

  async function handleFileUpload(
    e: ChangeEvent<HTMLInputElement>
  ): Promise<void> {
    const file = e.target.files?.item(0);
    const res = await fetchPresignedImage(file);
    await uploadImage(res?.signedRequest, file);
    setProfileImage(res?.url);
  }

  return [profileImage, handleFileUpload] as const;
}
