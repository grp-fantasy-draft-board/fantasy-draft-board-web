import { FocusEvent, useState } from 'react';
import formSchema from '../../validations/formSchema';
import { errMessages } from '../../errors/inputErrMessages';
import { InputData } from '../../interfaces/InputData';

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function useInputValidate(
  inputs: InputData,
  initialInputState: InputData
) {
  const [error, setError] = useState<InputData>(initialInputState);

  function validateData(name: string, value: string): boolean {
    return formSchema.get(name).isValidSync({ [name]: value });
  }

  async function setErrorMessage(
    name: string | undefined,
    value: string
  ): Promise<void> {
    if (name) {
      const isValid: boolean = validateData(name, value);
      inputs[name] && inputs[name].length > 0
        ? setError({ [name]: !isValid ? errMessages.get(name) : '' })
        : setError({ [name]: '' });
    }
  }

  async function handleBlur(e: FocusEvent<HTMLInputElement>): Promise<void> {
    const { name, value } = e.target;
    await setErrorMessage(name, value);
  }

  return [error, handleBlur] as const;
}
