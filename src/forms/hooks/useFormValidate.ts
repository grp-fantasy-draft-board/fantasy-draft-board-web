import { useEffect, useState } from 'react';
import { InputData } from '../../interfaces/InputData';
import formSchema from '../../validations/formSchema';

export default function useFormValidate(
  inputs: InputData,
  schema: string
): boolean {
  const [isDisabled, setIsDisabled] = useState<boolean>(true);

  useEffect(() => {
    const valid: boolean = formSchema.get(schema).isValidSync({ ...inputs });

    setIsDisabled(!valid);
  }, [inputs]);

  return isDisabled;
}
