import * as React from 'react';
import { useErrorHandler } from 'react-error-boundary';
import { useHistory } from 'react-router-dom';
import { api, auth } from '../../httpClient';
import { InputData } from '../../interfaces/InputData';

interface Error {
  isError: boolean;
  display: string;
}

interface SignIn {
  inputs: InputData;
  alert: Error;
}

// eslint-disable-next-line @typescript-eslint/explicit-module-boundary-types
export default function useForm(
  initialInputState: InputData,
  endpoint: string,
  routeRedirect?: string | undefined,
  image?: string | undefined
) {
  const history = useHistory();
  const handleError = useErrorHandler();
  const [{ inputs, alert }, setState] = React.useState<SignIn>({
    inputs: initialInputState,
    alert: { isError: false, display: 'none' }
  });

  function addImageToInputs(): void {
    const avatarIcon =
      'https://fantasy-draft-board-image-dev.s3.amazonaws.com/members/avatar-icon.png';

    if (endpoint.includes('/owners')) inputs.image = image ?? avatarIcon;
  }

  async function httpClient(inputs: InputData): Promise<void> {
    let res;

    switch (true) {
      case endpoint.includes('login'):
      case endpoint.includes('reset-password-request'):
        res = await auth.post(endpoint, inputs);
        sessionStorage.setItem('token', res.data.access_token);
        break;
      case endpoint.includes('resetToken'):
        await api.patch(endpoint, inputs);
        break;
      case endpoint.includes('origins'):
        res = await api.post(endpoint, inputs);
        sessionStorage.setItem('token', res.data.access_token);
        break;
    }
  }

  async function sendFormValues(): Promise<void> {
    addImageToInputs();
    try {
      await httpClient(inputs);
    } catch (err: any) {
      switch (err.response.status) {
        case 401:
          setState((prevState) => ({
            ...prevState,
            alert: { isError: true, display: 'block' }
          }));
          break;
        default:
          handleError(err);
          break;
      }
    }
  }

  async function handleSubmit(
    e: React.FormEvent<HTMLFormElement>
  ): Promise<void> {
    e.preventDefault();
    await sendFormValues();
    setState((prevState) => ({ ...prevState, inputs: initialInputState }));
    routeRedirect && history.push(routeRedirect);
  }

  function handleCloseClick(): void {
    setState((prevState) => ({
      ...prevState,
      alert: { isError: false, display: 'none' }
    }));
  }

  function handleChange(e: React.ChangeEvent<HTMLInputElement>): void {
    const { name, value } = e.target;
    setState((prevState) => ({
      ...prevState,
      inputs: { ...prevState.inputs, [name]: value }
    }));
  }

  return [inputs, handleChange, handleSubmit, alert, handleCloseClick] as const;
}
