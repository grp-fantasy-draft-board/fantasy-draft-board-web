import React from 'react';
import SignIn from './SignIn';
import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

describe.skip('<SignIn />', () => {
  beforeEach(() => {
    render(<SignIn />);
  });

  describe('when it renders successfully', () => {
    test('should show a title element', () => {
      const header = screen.getByRole('heading');
      expect(header).toBeInTheDocument();
    });

    test('should show form email input element', () => {
      const emailInputNode = screen.getByLabelText(/email/i);
      expect(emailInputNode.getAttribute('name')).toBe('email');
    });

    test('should show form password input element', () => {
      const passwordInputNode = screen.getByLabelText(/password/i);
      expect(passwordInputNode.getAttribute('name')).toBe('password');
    });

    test('should show form button element', () => {
      const buttonNode = screen.getByRole(/button/i);
      expect(buttonNode.getAttribute('type')).toBe('submit');
    });
  });

  describe('form element', () => {
    const email = 'email@example.com';
    const password = 'password';

    test('should have a valid email', () => {
      const emailInputNode = screen.getByPlaceholderText(/email/i);

      expect(emailInputNode.value).toBeEmpty();

      userEvent.type(emailInputNode, email);
      expect(emailInputNode.value).toBe('email@example.com');
    });

    test('should have a valid password', () => {
      const passwordInputNode = screen.getByPlaceholderText(/password/i);

      expect(passwordInputNode.value).toBeEmpty();

      userEvent.type(passwordInputNode, password);
      expect(passwordInputNode.value).toBe('password');
    });

    test.skip('should properly submit', async () => {
      const mockHandleLogin = jest.fn();
      const button = screen.getByRole(/button/i);
      userEvent.click(button);

      expect(mockHandleLogin).toHaveBeenCalled();

      // expect(onSubmit).toHaveBeenCalledWith({
      //   email,
      //   password
      // });
    });
  });
});
