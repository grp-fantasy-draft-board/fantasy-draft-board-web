import React from 'react';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import { ClassNameMap } from '@material-ui/core/styles/withStyles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import * as hooks from './hooks';
import { InputData } from '../interfaces/InputData';
import useStyles from './styles/useFormStyles';

export default function PasswordReset(): JSX.Element {
  const classes: ClassNameMap = useStyles();
  const query: URLSearchParams = hooks.useQuery();
  const memberId: string | null = query.get('memberId');
  const resetToken: string | null = query.get('token');
  const initialInputState: InputData = { password: '' };
  const endpoint = `owners/${memberId}/reset-password?resetToken=${resetToken}`;
  const [inputs, handleChange, handleSubmit] = hooks.useForm(
    initialInputState,
    endpoint
  );
  const [error, handleBlur] = hooks.useInputValidate(inputs, initialInputState);
  const isDisabled: boolean = hooks.useFormValidate(inputs, 'password');

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Reset Your Password
        </Typography>
        <form
          className={classes.form}
          autoComplete="off"
          onSubmit={handleSubmit}
        >
          <TextField
            required
            error={Boolean(error.password)}
            fullWidth
            variant="outlined"
            id="password"
            name="password"
            type="password"
            label="Password"
            placeholder="Password"
            margin="normal"
            value={inputs.password}
            onChange={handleChange}
            onBlur={handleBlur}
            helperText={error.password}
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={isDisabled}
          >
            Reset Password
          </Button>
        </form>
      </div>
    </Container>
  );
}
