import React from 'react';
import Button from '@material-ui/core/Button';
import Checkbox from '@material-ui/core/Checkbox';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { Link as RouterLink } from 'react-router-dom';
import useStyles from './styles/useFormStyles';
import { useForm, useFormValidate, useInputValidate } from './hooks';
import { InputData } from '../interfaces/InputData';
import AlertMessage from '../dialogs/Alert';

export default function SignIn(): JSX.Element {
  const classes = useStyles();
  const initialSignInState: InputData = { email: '', password: '' };
  const [inputs, handleChange, handleSubmit, alert, handleClose] = useForm(
    initialSignInState,
    '/login'
  );
  const [error, handleBlur] = useInputValidate(inputs, initialSignInState);
  const isDisabled = useFormValidate(inputs, 'signInSubmit');

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Sign In
        </Typography>
        <AlertMessage
          open={alert.isError}
          handleClick={handleClose}
          messageType="signIn"
          display={alert.display}
        />
        <form
          className={classes.form}
          autoComplete="off"
          onSubmit={handleSubmit}
        >
          <TextField
            required
            error={Boolean(error.email)}
            fullWidth
            autoFocus
            variant="outlined"
            id="email"
            name="email"
            type="email"
            label="Email"
            placeholder="Email"
            margin="normal"
            value={inputs.email}
            onChange={handleChange}
            onBlur={handleBlur}
            helperText={error.email}
          />
          <TextField
            required
            error={Boolean(error.password)}
            fullWidth
            variant="outlined"
            id="password"
            name="password"
            type="password"
            label="Password"
            placeholder="Password"
            margin="normal"
            value={inputs.password}
            onChange={handleChange}
            onBlur={handleBlur}
            helperText={error.password}
          />
          <FormControlLabel
            control={<Checkbox value="remember" color="primary" />}
            label="Remember me"
          />
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={isDisabled}
          >
            Sign In
          </Button>
          <Grid container>
            <Grid item xs>
              <Link
                component={RouterLink}
                to="/request-password-reset"
                variant="body2"
              >
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link component={RouterLink} to="/sign-up" variant="body2">
                Don&#39;t have an account? Sign Up
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}
