import React, { ChangeEvent } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import { makeStyles, Theme } from '@material-ui/core/styles';

const useStyles = makeStyles((theme: Theme) => ({
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.primary.light,
    width: theme.spacing(15),
    height: theme.spacing(15)
  },
  inputFile: {
    display: 'none'
  }
}));

interface ImageUploadProps {
  image: string | undefined;
  onChange: (e: ChangeEvent<HTMLInputElement>) => Promise<void>;
}

export default function ImageUpload({
  image,
  onChange
}: ImageUploadProps): JSX.Element {
  const classes = useStyles();

  return (
    <>
      <Avatar alt="image" src={image} className={classes.avatar} />
      <input
        accept="image/*"
        id="image"
        name="image"
        className={classes.inputFile}
        type="file"
        onChange={onChange}
      />
      <label htmlFor="image">
        <Button variant="contained" color="primary" component="span">
          Upload
        </Button>
      </label>
    </>
  );
}
