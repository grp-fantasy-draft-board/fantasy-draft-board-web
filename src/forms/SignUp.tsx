import React from 'react';
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import { Link as RouterLink } from 'react-router-dom';
import ImageUpload from './ImageUpload';
import useStyles from './styles/useFormStyles';
import * as hooks from './hooks';
import { InputData } from '../interfaces/InputData';

export default function SignUp(): JSX.Element {
  const classes = useStyles();
  const initialSignUpState: InputData = {
    firstName: '',
    lastName: '',
    email: '',
    password: ''
  };
  const [profileImage, handleFileUpload] = hooks.useImageUpload(
    'members',
    '/owners/image'
  );
  const [inputs, handleChange, handleSubmit] = hooks.useForm(
    initialSignUpState,
    '/owners',
    undefined,
    profileImage
  );
  const [error, handleBlur] = hooks.useInputValidate(
    inputs,
    initialSignUpState
  );
  const isDisabled = hooks.useFormValidate(inputs, 'signUpSubmit');

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <ImageUpload image={profileImage} onChange={handleFileUpload} />
        <form className={classes.form} onSubmit={handleSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="First Name"
                autoFocus
                value={inputs.firstName}
                onChange={handleChange}
                onBlur={handleBlur}
                error={Boolean(error.firstName)}
                helperText={error.firstName}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Last Name"
                name="lastName"
                value={inputs.lastName}
                onChange={handleChange}
                onBlur={handleBlur}
                error={Boolean(error.lastName)}
                helperText={error.lastName}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                value={inputs.email}
                onChange={handleChange}
                onBlur={handleBlur}
                error={Boolean(error.email)}
                helperText={error.email}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={inputs.password}
                onChange={handleChange}
                onBlur={handleBlur}
                error={Boolean(error.password)}
                helperText={error.password}
              />
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={isDisabled}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link component={RouterLink} to="/sign-in" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
    </Container>
  );
}
