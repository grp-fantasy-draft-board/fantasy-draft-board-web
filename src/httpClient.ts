import axios from 'axios';

const { REACT_APP_API_URL, REACT_APP_BASE_URL } = process.env;

export const api = axios.create({
  baseURL: REACT_APP_API_URL ?? ''
});

export const auth = axios.create({
  baseURL: REACT_APP_BASE_URL ?? ''
});
